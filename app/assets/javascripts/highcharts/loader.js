DataLoader = function() {
  self = this

  fetchSeries = function(callback) {
    $.get('/stat?fetch_data=true'
    ).done(function(series){
      callback(series);
    })
  }

  self.updateSeries = function(highchartsSeries) {
    fetchSeries(function(newSeries){

      _.each(highchartsSeries, function(hSeria){
        nSeria = _.find(newSeries, function(nSeria){
          return hSeria.name == nSeria.name
        });

        if(nSeria) {
          var x = (new Date()).getTime(),
              y = nSeria.data[0]

          console.log(nSeria)
          console.log(x, y)

          hSeria.addPoint([x, y], true, true)
        }
      });
    })
  }
}

dataLoader = new DataLoader
loadHigcharts(dataLoader);
