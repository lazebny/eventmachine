emptyPointsGenerator = function(size) {
  points = []

  for(var i=1; i < size; i++ ){
    var time = (new Date()).getTime()
    point = { x: time, y: 0 }
    points.push(point);
  }
  return points;
}

loadHigcharts = function (dataLoader) {
  $(function () {
    $('#container').highcharts({
       chart: {
         type: 'spline',
         animation: Highcharts.svg,
         marginRight: 10,
         events: {
           load: function () {
             var series = this.series;
             setInterval(function () {
               console.log(series)
               dataLoader.updateSeries(series);
             }, 1000);
           }
         }
       },
       title: { text: 'Service statistic' },
       subtitle: { text: 'test work' },


       xAxis: {
           type: 'datetime',
           tickPixelInterval: 150
       },

       yAxis: {
          title: {
              text: 'Requests to service'
          },
          plotLines: [{
              value: 0,
              width: 1,
              color: '#808080'
          }]
        },

        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },

        exporting: {
          enabled: false
        },
        series: [
         { name: 'all', data: emptyPointsGenerator(10) },
         { name: 'queue', data: emptyPointsGenerator(10) },
         { name: 'success', data: emptyPointsGenerator(10) }
       ]
       });
     });
  };
