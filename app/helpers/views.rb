module Helpers
  module Views
    def view_content(filename)
      content = ''

      filepath = [views_path, filename].join('/')
      f = File.open(filepath) do |f|
        content = f.read
      end

      content
    end

    def views_path
      File.expand_path('../../views', __FILE__)
    end

    def render(view)
      content = view_content view
      view_expressions.each do |key, value|
        content.gsub!("{{#{key}}}", value)
      end
      content
    end

    def view_expressions
      {}
    end

    def assets_path
      File.expand_path('../../assets', __FILE__)
    end

    def javascript_asset(filename)
      content = ''

      filepath = [assets_path, 'javascripts', filename].join('/')
      f = File.open(filepath) do |f|
        content = f.read
      end

      content
    end
  end
end
