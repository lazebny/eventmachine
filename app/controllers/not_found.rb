require File.expand_path('../base.rb', __FILE__)

module Controllers
  class NotFound < Base
    def execute(req, resp)
      resp.status = 404
      resp.send_response
    end
  end
end
