require File.expand_path('../../helpers/views.rb', __FILE__)

module Controllers
  class Base
    include Helpers::Views

    def execute(req, resp)
      raise 'Implement this method'
    end
  end
end
