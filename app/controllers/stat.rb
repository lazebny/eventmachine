require 'json'
require File.expand_path('../base.rb', __FILE__)
require File.expand_path('../../../lib/redis_store/all.rb', __FILE__)
require File.expand_path('../../../lib/redis_store/queue.rb', __FILE__)
require File.expand_path('../../../lib/redis_store/successed.rb', __FILE__)

module Controllers
  class Stat < Base
    def execute(req, resp)
      if req[:query_string] == 'fetch_data=true'
        resp.content_type 'text/json'
        resp.content = JSON[counters]

      else
        resp.content = render 'stat.html'
      end

      resp.status = 200
      resp.send_response
    end

    private

    def view_expressions
      {
        highcharts_config: javascript_asset('highcharts/config.js'),
        highcharts_loader: javascript_asset('highcharts/loader.js')
      }
    end

    def counters
      [
        {name: 'all', data: [::RedisStore::All.new.keys('*').size]},
        {name: 'queue', data: [::RedisStore::Queue.new.keys('*').size]},
        {name: 'success', data: [::RedisStore::Successed.new.keys('*').size]}
      ]
    end
  end
end
