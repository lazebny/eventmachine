require 'securerandom'
require File.expand_path('../base.rb', __FILE__)
require File.expand_path('../../../lib/redis_store/queue.rb', __FILE__)

module Controllers
  class Send < Base
    def execute(req, resp)
      resp.content_type 'text/json'
      resp.content = "OK"
      resp.status = 200
      resp.send_response

      request_uuid = SecureRandom.uuid
      url = parse_url req[:query_string]
      ::RedisStore::Queue.new.set(request_uuid, url) if url != ''
    end

    private

    def parse_url(query_string)
      return unless query_string

      query_string.downcase
        .split('&')
        .grep(/url=/)
        .first
        .to_s
        .split('=')[1].to_s.strip
    end
  end
end
