require 'eventmachine'
# Dir[File.expand_path('../lib/**/*.rb', __FILE__)].each { |file| require file }
require File.expand_path('../lib/server.rb', __FILE__)
require File.expand_path('../lib/queue_service.rb', __FILE__)
require File.expand_path('../lib/redis_store/all.rb', __FILE__)
require File.expand_path('../lib/redis_store/successed.rb', __FILE__)
require File.expand_path('../lib/redis_store/failed.rb', __FILE__)
require File.expand_path('../lib/redis_store/queue.rb', __FILE__)

EM::run do
  ::RedisStore::All.new.flushdb
  ::RedisStore::Successed.new.flushdb
  ::RedisStore::Failed.new.flushdb
  ::RedisStore::Queue.new.flushdb

  EM::start_server "127.0.0.1", 8081, Server
  EM::add_periodic_timer(1) do
    ::QueueService.new.execute unless ::QueueService.running
  end

  puts 'running echo server on 8081'
end
