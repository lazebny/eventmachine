require File.expand_path('../spec_helper.rb', __FILE__)

describe Router do
  let(:port) { 8081 }
  let(:domain) { '127.0.0.1' }
  let(:domain_full) { 'http://' + domain + ':' + port.to_s }
  it "not found" do
    EM.run {
      EM::start_server domain, port, Server

      http = EM::HttpRequest.new(domain_full + '/test').get

      http.callback {
        expect(http.response_header.status).to eql(404)

        EM::stop
      }
    }
  end

  it "send" do
    EM.run {
      EM::start_server domain, port, Server

      http = EM::HttpRequest.new(domain_full + '/send').get

      http.callback {
        expect(http.response_header.status).to eql(200)

        EM::stop
      }
    }
  end

  it "stat" do
    EM.run {
      EM::start_server domain, port, Server

      http = EM::HttpRequest.new(domain_full + '/stat').get

      http.callback {
        expect(http.response_header.status).to eql(200)
        expect(http.response).to match('highcharts')

        EM::stop
      }
    }
  end
end
