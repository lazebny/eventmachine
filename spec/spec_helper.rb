require 'simplecov'
SimpleCov.start

require 'eventmachine'
require 'evma_httpserver'
require 'em-http-request'
require 'rspec'
require 'pry'

Dir[File.expand_path('../../lib', __FILE__) + '/**/*.rb'].each {|file| require file }


RSpec.configure do |config|
   config.color = true
   config.formatter = :documentation # :progress, :html, :textmate
end

