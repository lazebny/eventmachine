require File.expand_path('../spec_helper.rb', __FILE__)
require 'securerandom'

describe ::QueueService do
  let(:queue_store) { ::RedisStore::Queue.new }
  let(:successed_store) { ::RedisStore::Successed.new }
  let(:failed_store) { ::RedisStore::Failed.new }
  let(:all_store) { ::RedisStore::All.new }

  let(:urls) {
    %w(
      http://yandex.com
      http://google.com
      http://mebeltorg.com/no_page
      http://githab.com
    )
  }

  before do
    queue_store.flushdb
    successed_store.flushdb
    failed_store.flushdb
    all_store.flushdb

    urls.each do |url|
      queue_store.set SecureRandom.uuid, url
    end
  end

  it "store has all keys" do
    expect(queue_store.keys('*').size).to eql(4)
  end

  describe "execute" do
    before do
      EM.run {
        EM::start_server '127.0.0.1', 8081, Server

        ::QueueService.new.execute

        EM::add_periodic_timer(3) do
           EM::stop
        end
        #unless ::QueueService.running
      }
    end

    describe 'stores' do
      it "all" do
        expect(all_store.keys('*').size).to eql(4)
      end

      it "sccessed" do
        expect(successed_store.keys('*').size).to eql(1)
      end

      # it "failed" do
      #   expect(failed_store.keys('*').size).to eql(3)
      # end

      it "queue" do
        expect(queue_store.keys('*').size).to eql(3)
      end
    end
  end
end
