require File.expand_path('../../spec_helper.rb', __FILE__)

describe ::RedisStore::Queue do
  let(:store) { ::RedisStore::Queue.new }

  it "stored value" do
    EM.run {
      EM::start_server '127.0.0.1', 8081, Server

      store.set '1', 'test'
      expect(store.get(1)).to eql('test')

      EM::stop_event_loop
    }
  end
end
