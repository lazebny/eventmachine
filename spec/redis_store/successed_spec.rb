require File.expand_path('../../spec_helper.rb', __FILE__)

describe ::RedisStore::Successed do
  let(:store) { ::RedisStore::Successed.new }

  it "stored value" do
    EM.run {
      EM::start_server '127.0.0.1', 8081, Server

      store.set '1', 'test'
      expect(store.get(1)).to eql('test')

      EM::stop_event_loop
    }
  end
end
