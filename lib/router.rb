files = Dir[File.expand_path('../../app/controllers', __FILE__) + '/**/*.rb']
files.each { |file| require file }

class Router
  def initialize(req, resp)
    @req = req
    @resp = resp
  end

  def execute
    controller.new.execute(@req, @resp)
  end

  private

  def controller
    begin
      ::Controllers.const_get controller_name
    rescue Exception
      ::Controllers::NotFound
    end
  end

  def controller_name
    @req[:request_uri].to_s
      .split('/')
      .select{ |el| el != '' }
      .map{ |el| el.capitalize }
      .join('::')
  end
end
