require 'eventmachine'
# require 'em-redis'
require 'redis'

module RedisStore
  class Base
    # def initialize(store = EM::Protocols::Redis)
    def initialize(store = Redis)
      @store = store.connect options
    end

    def method_missing(name, *args)
      @store.send(name, *args)
    end

    def options
      {
        host: 'localhost',
        port: 6379,
        db: db
      }
    end

    def db
      raise 'Implement this method'
    end
  end
end
