require 'eventmachine'
require 'em-http-request'
require File.expand_path('../redis_store/queue.rb', __FILE__)
require File.expand_path('../redis_store/all.rb', __FILE__)
require File.expand_path('../redis_store/successed.rb', __FILE__)
require File.expand_path('../redis_store/failed.rb', __FILE__) 
class QueueService
  @@running = false

  def self.running
    @@running
  end

  def execute
    @@running = true

    queue_store.keys('*').each do |key|

      url = queue_store.get key

      if all_store.keys(key).size == 0
        all_store.set key, url
      end

      options = {
        :connect_timeout => 2
      }

      http = EM::HttpRequest.new(url, options).get
      http.callback do

        # p http.response_header.status

        if http.response_header.status == 200
          success_process(key, url)
        # else
          # fail_process(key, url)
        end

        p ['success:', http.response_header.status.to_s, url].join ' '
      end

      http.errback do

        # fail_process(key, url)

        p ['error:', http.response_header.status.to_s, url].join ' '
      end
    end

    @@running = false
  end

  private

  def success_process(key, url)
    successed_store.set key, url
    queue_store.del key
  end

  def fail_process(key, url)
    failed_store.set key, url
    queue_store.del key
  end

  def queue_store
    @queue_store ||= ::RedisStore::Queue.new
  end

  def all_store
    @all_store ||= ::RedisStore::All.new
  end

  def successed_store
    @successed_store ||= ::RedisStore::Successed.new
  end

  def failed_store
    @failed_store ||= ::RedisStore::Failed.new
  end
end
