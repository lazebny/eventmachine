#!/usr/bin/env ruby
require 'rubygems'
require 'eventmachine'
require 'evma_httpserver'

require File.expand_path('../router.rb', __FILE__)

module Server
  include EventMachine::HttpServer

  def process_http_request
    resp = EventMachine::DelegatedHttpResponse.new(self)
    router = ::Router.new({
        request_method: @http_request_method,
        request_uri: @http_request_uri,
        query_string: @http_query_string,
        protocol: @http_protocol
      },
      resp
    )

    router.execute
  end
end
