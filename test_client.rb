require 'eventmachine'
require 'em-http-request'

class TestQuery
  URLS = %W(
    http://google.com
    http://fotomag.com.ua
    http://rozetka.com.ua
    http://fotos.ua
    http://gorod.dp.ua
    http://stackoverflow.com
  )

  def self.get
    url = URLS[Random.rand(URLS.size) - 1]
    query = '/send?url=' + url
    ::EM::HttpRequest.new('http://localhost:8081' + query).get
    p 'send url ' + url
  end
end

EM.run {
  # EM::HttpRequest.new('http://localhost:8081').get
  EM::add_periodic_timer(1) do
    TestQuery::get
  end
}
